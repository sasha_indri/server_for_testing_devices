
import socket
import json
import random

TCP_IP = '192.168.1.155'
TCP_PORT = 50
BUFFER_SIZE = 1024
MESSAGE = "Hello, World!"

def convert_string_to_json( data ):
    data_jason = None;
    #print("received data string : ", data)
    if not data: return False
    try:
        data_jason = json.loads(data)
    except ValueError:
        print("it's not json")
        return False
    #print("json: ", data_jason)
    return data_jason

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(json.dumps({'ready': 'true', 'status': 'Ok'}).encode())
print("Send data >>><<<< ")
cnt = 0
while True:
    try:
        data = s.recv(BUFFER_SIZE)
        print("received data:{}, {}".format(convert_string_to_json(data.decode('utf-8'))['long'], convert_string_to_json(data.decode('utf-8'))['data']))
        cnt = 0
        cnt1 = 0
        arr = []
        while(cnt< 3 * int(convert_string_to_json(data.decode('utf-8'))['long'])):
            arr.append(int(convert_string_to_json(data.decode('utf-8'))['command']))
            arr.append(int(convert_string_to_json(data.decode('utf-8'))['data'][cnt1]))
            arr.append(random.randint(0, 240))#int(convert_string_to_json(data.decode('utf-8'))['data'][cnt1+1])-1
            cnt += 3
            cnt1 += 2
        print(arr)
        s.send(json.dumps({'answer': str(int(convert_string_to_json(data.decode('utf-8'))['data'][1]) == random.randint(0, 240)), 'long': '1', 'data': arr}).encode())
    except socket.error as e:
        s.close()
        print("error <> ", e)
        break


