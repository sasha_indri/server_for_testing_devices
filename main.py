from flask import Flask, url_for
from flask import render_template, request, jsonify, json
from jinja2 import Template
import gen_table_bd
import cloud_connect
import socket, time, threading
#from multiprocessing import Process

TCP_IP = '0.0.0.0'
TCP_PORT = 50
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
conn, addr = None, None

global data_client_recive

recive_temp = None;
data_client_transmit = None


database = ""

app = Flask(__name__)
def read_write_buf(in_data):
    with open('buf.txt', "r") as f:
        temp = f.readlines()[0]
    with open('buf.txt', "w") as f:
            f.write(json.dumps(in_data))
            f.close()
    return convert_string_to_json(temp)


##---------------------------------------------------------------------##
@app.route("/")
def start_page():
    return render_template("start_page.html")
    #return render_template("index.html")

##---------------------------------------------------------------------##
@app.route("/simp_test")
def simple_test():
    return render_template("index.html")

@app.route("/hello") #
def hello_page():
    url_for('static', filename='style.css')
    html = open('templates/index.html').read()
    template = Template(html)
    return template.render(table = gen_table_bd.gen_html_table())#render_template('index.html',)

##---------------------------------------------------------------------##
@app.route("/test_p")
def test_p():
    return render_template('test.html')

##---------------------------------------------------------------------##
@app.route("/test", methods = ['POST'])
def test():
    #print(request.is_json)
   # content = request()
    #print(content)
    return json.dumps({'status':'OK'});

##---------------------------------------------------------------------##
@app.route("/page_test_device", methods = ['POST']) # {"Name_Table": "test1", "Name_element": "Name", "Result_Test": "ResTest", "Serial_number": "Serial", "Test_Information": "TestInf", "Frequency_test": True}
def page_testered():
    print("it's json >> ",request.is_json)
    content = request.get_json()
    #print("json post>> ", content)
    global data_client_transmit
    global Valid_data
    global table_db
    global error_type
    global use_DB
    error_type = "No error"
    table_db = content['Name_Table']
    Valid_data = False
    use_DB = "mydevice"
    data_client_transmit_cloud = [];
    try:
        dump = str([int(content['Test_Information']), int(content['Result_Test'])])
        if(int(content['Test_Information']) > 255 or int(content['Result_Test'])> 255 ):
            Valid_data = False
            error_type = str(content['Test_Information'] + " or " + content['Result_Test'] + " > 255 Error ")
            return json.dumps(content)
    except Exception:
        Valid_data = False
        error_type = str(content['Test_Information']+"  "+ content['Result_Test'] + "  <<< Not integer ")
        return json.dumps(content)
    if(content['Frequency_test'] == True):
        data_client_transmit = "{ \"command\": \"30\", \"long\": \"1\", \"data\": "+dump+"}"
    else:
        data_client_transmit = "{ \"command\": \"10\", \"long\": \"1\", \"data\": " + dump + "}"

    print(data_client_transmit)
    data_client_transmit_cloud = data_client_transmit
    time.sleep(4)
    data_client_transmit = None;
    temp = read_write_buf({'Status': 'Fail'})

    try:
        if(temp['Status'] == "Fail"):
            Valid_data = False
            error_type = "Connection lost, check device"
            return json.dumps(temp)
        print("status = ",temp['Status'])
    except BaseException:
        print("Recive data from TCPClient: ",temp)
        Valid_data = True

    #cloud_connect.send_masege_to_cloud(data_client_transmit,str(content))
    #{Serial_number: serialNumber, Name_element: NameElem, Test_Information: TestInform, Result_Test: ResTestI,
    # Name_Table: NameTable}      data_base, db_table, name_d, serial_d, test_data, ansver_data, result_data
    use_DB = content['Name_DB'] #"mydevice"
    gen_table_bd.insert_in_table(use_DB, content['Name_Table'], content['Serial_number'], content['Name_element'], str(temp['data'][0]), str(temp['data'][2]), str(temp['data'][1]==255) ) #content['Result_Test']"True"
    print(content['Name_Table'])
    cloud_connect.send_masege_to_cloud(str(data_client_transmit_cloud), str(content))
    return json.dumps(temp)#{'date':'Ok'}

##---------------------------------------------------------------------##
@app.route("/get_table")
def get_table():
    time.sleep(1)
    if(Valid_data):
        return gen_table_bd.gen_html_table(DB = use_DB,BD_table=table_db)
    else:
        return "<p> "+error_type+" </p> "+gen_table_bd.gen_html_table(DB = use_DB,BD_table=table_db)

##---------------------------------------------------------------------##
def convert_string_to_json( data ):
    data_jason = None;
    #print("received data string : ", data)
    if not data: return False
    try:
        data_jason = json.loads(data)
    except ValueError:
        print("it's not json")
        return False
    #print("json: ", data_jason)
    return data_jason

##---------------------------------------------------------------------##
@app.route("/create_db")
def create_db_page():
    return render_template("create_db.html")

##---------------------------------------------------------------------##
@app.route("/create_db_interface", methods = ['POST']) # {"Name_DB": "test1"}
def create_db_interface():
    print("it's json >> ",request.is_json)
    content = request.get_json()
    print(" json = ", content)

    return json.dumps({'data' : gen_table_bd.creat_db(DB=content["Name_DB"])})
##---------------------------------------------------------------------##
@app.route("/create_db_reference")
def create_db_reference_page():
    return render_template("create_reference.html")
##---------------------------------------------------------------------##
@app.route("/create_db_reference_interface", methods = ['POST']) # {"Name_DB": "test1", BitUse: 4}
def create_db_reference_interface():
    print("it's json >> ",request.is_json)
    content = request.get_json()
    print(" json = ", content)
##---------------------------------------------------------------------##
@app.route("/test_db_reference")
def test_db_reference_page():
    return render_template("test_reference.html")
##---------------------------------------------------------------------##
@app.route("/test_db_reference_interface", methods = ['POST']) # {"Name_DB": "test1", BitUse: 4}
def test_db_reference_interface():
    print("it's json >> ",request.is_json)
    content = request.get_json()
    print(" json = ", content)
##---------------------------------------------------------------------##
@app.route("/drop_db_interface", methods = ['POST']) # {"Name_DB": "test1"}
def drop_db_interface():
    print("it's json >> ", request.is_json)
    content = request.get_json()
    print(" json = ", content)

    return json.dumps({'data' : gen_table_bd.drop_db(content["Name_DB"])})
##---------------------------------------------------------------------##
def socet_server():
    msg_status = {}
    read_write_buf({"Status": "Fail"})
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    #s.setblocking(False)
    conn, addr = s.accept()
    print('Connection address:', addr)
    time.sleep(1)
    msg_status = (conn.recv(BUFFER_SIZE)).decode('utf-8')
    print("status dev >> ", msg_status)
    while(1):
        data = None
        #print(" ?>?>>>", data_client_transmit) {"ansver":"true", "long":"1", data}
        if ((data_client_transmit != None)):
            print(" Transmit data >>", data_client_transmit)
            try:
                    #print("json transmit: ", data_client_transmit)
                    conn.send(data_client_transmit.encode())
                    time.sleep(2)
                    data = (conn.recv(BUFFER_SIZE))
                    print("recive data = >>",data)
            except BaseException:
                read_write_buf({"Status": "Fail"})
                #s.close() # ?
                conn, addr = s.accept()
                print('Error reconnect. Connection address:', addr)
                time.sleep(1)
                msg_status = (conn.recv(BUFFER_SIZE)).decode('utf-8')# problem
                print("status dev >> ", msg_status)
                pass
        else:
            time.sleep(2)
        #print("ffff",data)
        if(data):
            data_client_recive = convert_string_to_json(data.decode('utf-8'))
            with open('buf.txt', "w") as f:
                f.write(json.dumps(data_client_recive))
                f.close()
                #print("json recive: ", data_client_recive)
                time.sleep(4)
        else:
            time.sleep(1)

##---------------------------------------------------------------------##
if __name__ == "__main__":

    t = threading.Thread(target=socet_server)
    t.daemon = True
    t.start()


    app.run(debug=True, host='0.0.0.0', port=80, use_reloader=False)#
   #gen_table_bd.create_table("test1")
   #gen_table_bd.insert_in_table("mydevice","test1" , "ds", "dsdsw32", "35", "43", "True")
   #print(gen_table_bd.gen_html_table(BD_table="test1"))