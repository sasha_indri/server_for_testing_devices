from flask_table import Table, Col
import mysql.connector
DBg = "mydevice"
BD_tableg = "test_device"
myhostg = "localhost"
myuserg="user_py"
mypasswdg="123"

def show_any(cursor):
  for x in cursor:
    print(x)

def creat_db(DB = "mydevice", BD_table = "test_device",myhost = "localhost", myuser="user_py", mypasswd="123"):
    DBg = DB
    BD_tableg = BD_table
    myhostg = myhost
    myuserg = myuser
    mypasswdg = mypasswd
    database = mysql.connector.connect(
        user=myuserg,
        passwd=mypasswdg,
        host=myhostg
    )
    db_cursor = database.cursor()

    db_cursor.execute("SHOW DATABASES")

    if(DBg in db_cursor):
        return "Data Base is exist! Please set again!"

    for db in db_cursor:
        print("data base " + db)
    print("Name DB = ", DB)
    db_cursor.execute("CREATE DATABASE " + DB)
    db_cursor.execute("USE "+DB)
    create_table(BD_tableg, DB)
    db_cursor.execute("SHOW DATABASES")

    items = [];
    i = 0
    for x in db_cursor:
      items.append(dict(id = i, Name_db = x[0]))# создаём словарь и заполняем лист словорями
      i += 1

    class ItemTable(Table): # структура таблицы
        id = Col('id')
        Name_db = Col('Name_db')
    table = ItemTable(reversed(items))
    html_table = str(table.__html__())
    print(html_table)
    database.commit()
    return html_table

def create_table(name_tadble, DB):
    database = mysql.connector.connect(
        user=myuserg,
        passwd=mypasswdg,
        database=DB,
        host=myhostg
    )
    db_cursor = database.cursor()
    sql = "CREATE TABLE "+name_tadble+" (id INT NOT NULL AUTO_INCREMENT, name CHAR(30), serial CHAR(30), test CHAR(30), answer CHAR(30), result CHAR(30), PRIMARY KEY (id))"
    var = ('name', 'serial', 'test', 'answer', 'result');
    db_cursor.execute(sql)#,var
    database.commit()
    db_cursor.execute("SHOW TABLES")
    for table in db_cursor:
        print(table)

    db_cursor.execute("SELECT * FROM " + name_tadble)
    myresult = db_cursor.fetchall()
    print(myresult)

def drop_db(DBg):
    database = mysql.connector.connect(
        user=myuserg,
        passwd=mypasswdg,
        host=myhostg
    )
    db_cursor = database.cursor()#buffered=True


    #if((DBg in db_cursor)):
        #pass
    #else:
        #return "Data Base is not exist! Please set again!"
    print("Name DB = ", DBg)
    try:
        sql = "DROP DATABASE " + str(DBg)# IF EXISTS
        db_cursor.execute(sql)
        database.commit()
    except BaseException:
        return "Data Base is not exist! Please set again!"
    db_cursor.execute("SHOW DATABASES")


    items = [];
    i = 0
    for x in db_cursor:
        items.append(dict(id=i, Name_db=x[0]))  # создаём словарь и заполняем лист словорями
        i += 1
    class ItemTable(Table):  # структура таблицы
        id = Col('id')
        Name_db = Col('Name_db')

    table = ItemTable(reversed(items))
    html_table = str(table.__html__())
    print(html_table)
    database.commit()
    return html_table

def gen_html_table(DB = "mydevice", BD_table = "test_device",myhost = "localhost"):
    mydb = mysql.connector.connect(
      host=myhost,
      user=myuserg,
      passwd=mypasswdg,
      database = DB
    )
    try:
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM "+ BD_table)
    except BaseException:
        return "Error no table or data base!"
    myresult = mycursor.fetchall()
    print("gen table = ", myresult)
    items = [];

    for x in myresult:
      items.append(dict(id = x[0],serial = x[1], name = x[2], test = x[3], ansver = x[4],result = x[5]))# создаём словарь и заполняем лист словорями

    class ItemTable(Table): # структура таблицы
        id = Col('id')
        serial = Col('serial')
        name = Col('name')
        test = Col('test')
        ansver = Col('ansver')
        result = Col('result')

    table = ItemTable(reversed(items))

    return table.__html__()

def insert_in_table(data_base, db_table, name_d, serial_d, test_data, ansver_data, result_data):
    database = mysql.connector.connect(
        user=myuserg,
        passwd=mypasswdg,
        database=data_base,
        host=myhostg
    )
    db_cursor = database.cursor()
    sql = "INSERT INTO "+db_table+" (name, serial ,test, answer, result) VALUES (%s, %s, %s, %s, %s)"
    val = (name_d, serial_d, test_data, ansver_data, result_data)
    db_cursor.execute(sql, val)
    database.commit()
    db_cursor.execute("SELECT * FROM " + db_table)
    myresult = db_cursor.fetchall()
    print("table = ",myresult)